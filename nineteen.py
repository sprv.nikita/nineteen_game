#-*- coding: utf-8 -*-

__author__ = "sprv"

import os

deck = [[1,2,3,4,5,6,7,8,9], [1,1,1,2,1,3,1,4,1], [5,1,6,1,7,1,8,1,9]]

#добавляем все в конец (переписать бы в while.. .)
def add_str():
    lastRow = len(deck)-1
    lastElem = len(deck[len(deck)-1])

    for row in range(len(deck)):
        for elem in range(len(deck[row])):
            if (row == lastRow and elem == lastElem): return 0
            if (deck[row][elem] != 0):
                if (len(deck[len(deck)-1]) == 9): deck.append([])
                deck[len(deck)-1].append(deck[row][elem])

#вывод доски
def print_deck():
    os.system('color')
    rownum = 97
    print(' ' + '\x1b[0;30;41m' + ' 1 2 3 4 5 6 7 8 9 ' + '\x1b[0m')
    for row in deck:  
        print('\x1b[0;30;41m' + chr(rownum) + '\x1b[0m' + ' ' + ' '.join([str(elem).replace('0', ' ' ) for elem in row]))
        rownum += 1
    print('\n')

def check_true(fy, fx, sy, sx):
    #на всякий сортируем
    if ((fy > sy) or ((fy == sy) and (fx > sx))):
        fy, sy = sy, fy
        fx, sx = sx, fx

    #пусто ли между?
    if (fy == sy and sx-fx != 1):
        for elem in range(fx+1, sx):
            if(deck[fy][elem] != 0): return 1

    if (fx == sx and sy-fy != 1):
        for row in range(fy+1, sy):
            if(deck[row][fx] != 0): return 1

    #пусто ли с переносом строк
    if (fy != sy and fx != sx):
        for elem in range(fx+1, 9):    
            if(deck[fy][elem] != 0): return 1
        for elem in range(0, sx):    
            if(deck[sy][elem] != 0): return 1
        for row in range(fy+1, sy):
            for elem in range(0, 9):
                if(deck[row][elem] != 0): return 1

    #проверка под правила
    if (deck[fy][fx] != deck[sy][sx] and deck[fy][fx] + deck[sy][sx] != 10): return 2

    #все проверки пройдены
    return 0

def del_items(fy, fx, sy, sx):
    fy = int(ord(fy))-97
    fx = int(fx)-1
    sy = int(ord(sy))-97
    sx = int(sx)-1
    deck[fy][fx], deck[sy][sx] = 0, 0

def input_check(fy, fx, sy, sx):
    if (fy.isdigit() or sy.isdigit()): return 1 #буква

    #преобразуем все в нормальные значения (отсчет от 0)
    try:
        fy = int(ord(fy))-97
        fx = int(fx)-1
        sy = int(ord(sy))-97
        sx = int(sx)-1
    except ValueError: return 2
    except TypeError: return 2

    if ((fy not in range(0, len(deck))) or (sy not in range(0, len(deck)))): return 3  #диапазон значений
    if ((fx not in range(0, 9)) or (sx not in range(0, 9))): return 3  #диапазон значений
    if (deck[fy][fx] == 0 or deck[sy][sx] == 0): return 4 #неноль

    #все проверки пройдены
    return check_true(fy, fx, sy, sx)

#есть ли ходы
def move_exsist():
    gameover = 0
    for row in range(len(deck)):
        for elem in range(len(deck[row])):
            if (deck[row][elem] == 0):
                gameover += 1
                continue
            
            #проверка со всеми следующими
            f_elem2 = elem + 1
            for row2 in range(row, len(deck)):
                if (row2 != row): f_elem2 = 0
                for elem2 in range(f_elem2, len(deck[row2])):
                    if (deck[row2][elem2] == 0): continue
                    if not (check_true(row, elem, row2, elem2)): return 1
    
    #нет возможных ходов
    if (gameover == (len(deck)*9) - (9 - len(deck[len(deck)-1]))): return 2 #игра закончена
    return 0


if __name__ == "__main__":
    moveExsist = 1
    print_deck()
    while (moveExsist != 2):
        print("введите кординаты в формате 'a 1 b 2':")
        try:
            fy, fx, sy, sx = input().split()
        except KeyboardInterrupt:
            print('\x1b[1;32m' + 'GOODBYE' + '\x1b[0m')
            raise SystemExit
        except ValueError: print('\x1b[1;31m' + 'FALSE:' + '\x1b[0m' + 'неверное кол-во значений \n')
        else:
            if not input_check(fy, fx, sy, sx):
                #меняем на нули
                del_items(fy, fx, sy, sx)
                print('\x1b[1;32m' + 'OK' + '\x1b[0m' + '\n')
                #добавляем строку, если нет ходов
                moveExsist = move_exsist()
                if (moveExsist == 0):
                    add_str()
            else: print('\x1b[1;31m' + 'FALSE:' + '\x1b[0m' + ' проверьте значения, что-то не так... \n')
        print_deck()
    
    #вышли из while - выйграли
    print('\x1b[1;32m' + 'GAME OVER - ВЫ ВЫЙГРАЛИ' + '\x1b[1m')